<h1>ABOUT</h1>

In this project, a web based tool for predicting user travel modes based on machine learning algorithms is developed.
Although different machine learning algorithms like KNN, SVM, GBOOST, XGBOOST and Random Forest have all been 
trained with data gotten from [Geo Life Dataset](https://www.microsoft.com/en-us/research/publication/understanding-transportation-modes-based-on-gps-data-for-web-applications/), only KNN is integrated with the platform at the moment.

The architecture of the project is presented below. 


<img
src="ay-clusters.png"
width="300px"
alt="Subject Pronouns"
style="margin-right: 10px;"
/>

<h1>Development</h1>
1. Python (Django) backend Development <br /> 
2. ReactJS for frontend





<h1>Deployment</h1>
- pip install django djangorestframework  <br /> 
- python manage.py migrate(navigate to root folder and run migration)  <br /> 
- npm i (navigate to frontend folder and install dependencies)  <br /> 
- npm run dev  <br /> 
- python manage.py runserver  <br />
