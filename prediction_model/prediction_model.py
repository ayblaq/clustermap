import pickle
import os


def predict_mode(data, model):
    if model == "knn":
        return knn(data)


def knn(data):
    columns = ['Accl_avg', 'Accl_med', 'Accl_max', 'Vel_avg', 'Vel_med', 'Vel_max', 'Acceleration', 'Velocity', 'Distance', 'Time_delta']
    # data = data.dropna(subset=columns)
    x_values = data[columns].values
    scaler, label = get_encoders()
    x_values = scaler.fit_transform(x_values)
    model = load_model("knn")
    predicted_modes = model.predict(x_values)
    return label.inverse_transform(predicted_modes)


def get_encoders():
    scaler = load_pickle(os.getcwd()+'/prediction_model/labels/scaler.pkl')
    label = load_pickle(os.getcwd()+'/prediction_model/labels/labeler.pkl')
    return scaler, label


def load_pickle(file):
    pkl = pickle.load(open(file, 'rb'))
    return pkl


def load_model(model):
    loaded_model = pickle.load(open(os.getcwd()+"/prediction_model/models/{model}.pkl".format(model=model), 'rb'))
    return loaded_model


