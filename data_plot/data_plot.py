import base64
import io

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import metrics


plt.style.use('ggplot')
from mpl_toolkits.basemap import Basemap


def plot_data(df):
    plt.plot(df['xvalues'], df['yvalues'], label="{title}".format(title=df['xtitle']), marker='X', linestyle='-')
    plt.legend()
    plt.ylabel('{label}'.format(label=df['ylabel']), fontsize=14)
    plt.xlabel('{label}'.format(label=df['xlabel']), fontsize=14)
    ax = plt.gca()

    return plt, ax


def plot_centroid_circles(df, centroids, labels, title, col = ['Latitude', 'Longitude']):
    data = df[['Latitude', 'Longitude']].values
    lat_center, lon_center = zip(*centroids)
    centroids_df = pd.DataFrame({'Longitude': lon_center, 'Latitude': lat_center})
    fig, ax = plt.subplots()
    cmap = plt.get_cmap('Dark2')
    norm = plt.Normalize(vmin=0, vmax=len(centroids))
    dbscan_scatter_plot = ax.scatter(df['Latitude'], df['Longitude'], c=cmap(norm(labels)))
    centroid_scatter_plot = ax.scatter(centroids_df['Latitude'], centroids_df['Longitude'], marker='x', linewidths=2,
                                       c='k',
                                       s=50)
    ax.set_title(title, fontsize=7)
    ax.set_xlabel(col[0], fontsize=7)
    ax.set_ylabel(col[1], fontsize=7)
    ax.legend([dbscan_scatter_plot, centroid_scatter_plot], ['Cluster', 'Cluster Centroid'], loc='upper right',
              fontsize=6)

    # ax.set_aspect(1)
    for ind, i in enumerate(centroids):
        try:
            class_inds = np.where(labels == ind)[0]
            col = cmap(norm(ind))
            max_dist = np.max(metrics.pairwise_distances(i.reshape(1, -1), data[class_inds],
                                                         metric="euclidean"))
            ax.add_patch(plt.Circle(i, max_dist, linewidth=2, fill=False, color=col, clip_on=False))
        except:
            continue

    ax.autoscale(enable=True)
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    image_base64 = base64.b64encode(buf.getvalue()).decode('utf-8').replace('\n', '')
    buf.close()

    return image_base64


def plot_map(ax, df):
    lat = list(df['Latitude'])
    lon = list(df['Longitude'])
    margin = 0.01
    lat_min = min(lat) - margin
    lat_max = max(lat) + margin
    lon_min = min(lon) - margin
    lon_max = max(lon) + margin

    m = Basemap(llcrnrlon=lon_min,
                llcrnrlat=lat_min,
                urcrnrlon=lon_max,
                urcrnrlat=lat_max,
                lat_0=(lat_max - lat_min) / 2,
                lon_0=(lon_max - lon_min) / 2,
                projection='lcc',
                resolution='f', )

    m.drawcoastlines()
    m.fillcontinents(lake_color='aqua')
    m.drawmapboundary(fill_color='white')
    m.drawrivers()

    # plot points
    clist = list(df['cluster'].unique())
    print(clist)
    if -1 in clist:
        clist.remove(-1)
    k = len(clist)
    colors = iter(cm.Set1(np.linspace(0, 1, max(10, k))))
    for i in range(k):
        color = next(colors)
        df = df.loc[df['cluster'] == clist[i]]
        print("Cluster {} has {} samples.".format(clist[i], df.shape[0]))

        lons, lats = m(list(df['Longitude']), list(df['Latitude']))
        ax.scatter(lons, lats, marker='o', color=color, edgecolor='gray', zorder=5, alpha=1.0, s=15)
        # ax.show()
        plt.show()

