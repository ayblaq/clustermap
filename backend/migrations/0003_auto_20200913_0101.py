# Generated by Django 2.2.12 on 2020-09-12 22:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0002_dataset'),
    ]

    operations = [
        migrations.DeleteModel(
            name='BackEnd',
        ),
        migrations.DeleteModel(
            name='Dataset',
        ),
    ]
