from django.http import JsonResponse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view, parser_classes
from .models import Dataset
from .serializers import DatasetSerializer
from data_analyse import data_analysis
from data_analyse.data_filter import data_filter
from data_analyse.data_restructure import data_restructure
from data_analyse.data_cluster.data_cluster import cluster
from prediction_model import prediction_model
import pandas as pd
import os

default_title = "10a0ff73-9b7e-42b3-be60-367eac3bba3e"
default_clusters = ['kmeans', 'pca', 'dbscan']


@api_view(['POST', 'GET'])
def upload_data(request):
    if request.method == 'POST':
        serializer = DatasetSerializer(data=request.data)
        valid = data_filter.validate_file(request)
        if not valid:
            return Response("Only CSV file is allowed", status=status.HTTP_400_BAD_REQUEST)
        if serializer.is_valid():
            serializer.save()
            data = serializer.data
            return Response(get_default_structure(data), status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'GET':
        data = Dataset.objects.get(title=default_title)
        data_serializer = DatasetSerializer(data).data

        return Response(get_default_structure(data_serializer), status=status.HTTP_200_OK)


@api_view(['GET'])
def get_analysed_data(request, title):
    try:
        data = Dataset.objects.get(title=title)
    except Dataset.DoesNotExist:
        return JsonResponse({'message': 'The Data does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        data_serializer = DatasetSerializer(data).data
        new_df = data_restructure.recreate_df_cols(title, read_file(data_serializer['file']))
        structure = {'data': new_df.head(10)}

        return Response(structure, status=status.HTTP_200_OK)


@api_view(['POST'])
def get_cluster_plot(request, title):
    try:
        data = Dataset.objects.get(title=title)
    except Dataset.DoesNotExist:
        return JsonResponse({'message': "File does not exist"}, status=status.HTTP_404_NOT_FOUND)

    cluster_name = request.data.get("cluster")
    if cluster_name is None or cluster_name not in default_clusters:
        return JsonResponse({'message': 'Cluster type does not exist'}, status=status.HTTP_404_NOT_FOUND)
    data_serializer = DatasetSerializer(data).data
    df = read_file(data_serializer['file'])
    plot = cluster(cluster_name, request.data, df)
    structure = {'cluster_plot': plot}
    return Response(structure, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_predicted_modes(request, title, model):
    try:
        Dataset.objects.get(title=title)
    except Dataset.DoesNotExist:
        return JsonResponse({'message': "File does not exist"}, status=status.HTTP_404_NOT_FOUND)

    file = "/dataset/formatted/{file}.csv".format(file=title)
    df = read_file(file)
    # hard coded for now
    model = "knn"
    df['Modes'] = prediction_model.predict_mode(df, model)
    df = df[['Origin','Destination', 'Modes']]
    structure = {'data': df.head(20)}
    return Response(structure, status=status.HTTP_200_OK)


def read_file(filename):
    return pd.read_csv(os.getcwd() + filename)


def get_default_structure(serialized):
    structure = {'title': serialized['title']}
    data = read_file("{filename}".format(filename=serialized['file']))
    structure['data'] = data
    structure['statistics'] = {
        'Data Matrix': data_analysis.data_matrix_stats(data),
        'Missing values (NAs)': data_analysis.null_stats(data)}

    return structure
