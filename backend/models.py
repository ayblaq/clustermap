from django.db import models
import uuid
import os

directory_string_var = 'dataset/'


def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    instance.title = uuid.uuid4()
    filename = "%s.%s" % (instance.title, ext)

    return os.path.join(directory_string_var, filename)


class Dataset(models.Model):
    file = models.FileField(upload_to=get_file_path)
    title = models.CharField(max_length=100, default='')