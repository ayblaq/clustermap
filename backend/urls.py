from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^api/dataset$', views.upload_data),
    url(r'^api/dataset/(?P<title>[-\w]+)/$', views.get_analysed_data),
    url(r'^api/cluster/(?P<title>[-\w]+)/$', views.get_cluster_plot),
    url(r'^api/predict/(?P<title>[-\w]+)/model/(?P<model>[-\w]+)/$', views.get_predicted_modes),
]
