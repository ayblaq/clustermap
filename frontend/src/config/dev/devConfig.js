const devConfig = {
    apiUrl: 'http://127.0.0.1:8000/api/',
    country: 'EST',
    language: 'EN',
};

export default devConfig;
