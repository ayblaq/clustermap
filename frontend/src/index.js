import React from 'react';
import { render } from 'react-dom'
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';
import { Root } from '../src/app/components'
import { Routing, history} from '../src/routing'
import { store } from './store'
// import './styles/index.scss'
import 'semantic-ui-css/semantic.min.css';

if (process.env.NODE_ENV === 'production') {
    require('./pwa')
} else if (process.env.NODE_ENV === 'development') {
    console.log("This is for development")
}

render(
    <Provider store={store}>
        <Root routes={Routing} history={history} store={store} rKey={Math.random()}/>
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();
