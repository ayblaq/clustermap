import React from 'react';
import   {Container, Divider,
  Grid, Header, Image, List,
  Segment } from 'semantic-ui-react';


const Footer = () => {
    return (

        <Segment inverted vertical style={{margin: '2em 0em 0em', padding: '2em 0em'}}>
            <Container textAlign='center'>
                <Grid divided inverted stackable>
                    <Grid.Column width={3}>
                        <Header inverted as='h4' content='Contact'/>
                        <List link inverted>
                            <List.Item as='a'>Link Ones</List.Item>
                        </List>
                    </Grid.Column>
                    <Grid.Column width={12}>
                        <Header inverted as='h4' content='About'/>
                        <div>
                            <p>This is a web tool for analysing and clustering GPS trajectory. It also  predicts transport modes using different machine learning algorithm.</p>
                            <strong>Ayobami, Adewale</strong> and <strong>Amnir, Hadachi</strong>
                        </div>
                    </Grid.Column>
                </Grid>
            </Container>
        </Segment>

    )
}

export default Footer;