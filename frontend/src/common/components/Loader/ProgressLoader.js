import React from 'react'
import { Loader } from 'semantic-ui-react'
import './ProgressLoader.scss'

const ProgressLoader = (props) => {
  const {size, description} = props;

  return (
    <Loader active inline='centered' size={`${ size ? size : 'massive'}`}>
        { `${description ? description : 'Loading'}` }
    </Loader>
  )
}

export default ProgressLoader;
