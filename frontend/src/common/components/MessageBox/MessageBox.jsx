import React from 'react'
import {Message, Icon} from 'semantic-ui-react'
import './MessageBox.scss'

const MessageBox = (props) => {
    const {type, details} = props;

    return (
        <Message warning attached='bottom'>
            <Icon name={type}/>
            {details}
        </Message>
    )
}

export default MessageBox;
