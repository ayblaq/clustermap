import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { analysisReducers } from "../components/redux/reducers";
import { dataReducers } from "../components/redux/reducers";

export const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  data: dataReducers,
  analysis: analysisReducers
});
