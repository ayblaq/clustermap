import React from "react";
import {
    Container, Tab
} from 'semantic-ui-react'
import './HomeContainer.scss'
import {Footer, SiteHeader} from '../../common/components'
import {IntroTab, ImportTab, PreprocessTab, ClusterTab, PredictorTab} from "../../components";


const HomeContainer = () => {
    const panes = [
        {
            menuItem: 'Introduction',
            render: () => <Tab.Pane><IntroTab/></Tab.Pane>
        },
        {
            menuItem: 'Data import',
            render: () => <Tab.Pane><ImportTab/></Tab.Pane>
        },
        {
            menuItem: 'Data pre-processing',
            render: () => <Tab.Pane><PreprocessTab/></Tab.Pane>
        },
        {
            menuItem: 'Data Clustering',
            render: () => <Tab.Pane><ClusterTab></ClusterTab></Tab.Pane>
        },
        {
            menuItem: 'Predict Transport Mode',
            render: () => <Tab.Pane><PredictorTab></PredictorTab></Tab.Pane>
        }
    ]
    return (
        <div>
            <SiteHeader/>
            <Container className={"view-container"}>
                <Tab panes={panes} className="pane-tab"></Tab>
            </Container>
            <Footer/>
        </div>
    )
}

export default HomeContainer;
