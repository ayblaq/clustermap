import React, {useState} from 'react';
import {Message, Icon} from 'semantic-ui-react';
import {ClusterToolbar} from "../index";
import './ClusterTab.scss'
import {fetchClusterPlotApi} from "../api/api";
import {ProgressLoader} from "../../common/components";
import {useSelector} from "react-redux";


const ClusterTab = () => {

    const {
        data: {title}
    } = useSelector(state => state.data)

    const [options, setOptions] = useState({plot: null, loading: false, error: false})

    const fetchPlot = (event, settings) => {
        if (title) {
            settings['cluster'] = settings['active']
            setOptions({...options, loading: true, error: false})
            fetchClusterPlotApi(title, settings).then(res => {
                setOptions({...options, loading: false, error: false, plot: res['cluster_plot']})
            }).catch(error => {
                setOptions({...options, loading: false, error: true, plot: null})
            })
        }
    }

    const renderPlot = () => {
        if (options.loading) {
            return <ProgressLoader description={"generating plot"} size={"small"}/>
        } else if (options.error) {
            return <Message warning attached='bottom'>
                <Icon name='warning'/>
                Error
            </Message>
        } else if (options.plot) {
            return <div>
                <img src={`data:image/png;base64, ${options.plot}`}></img>
            </div>
        }
    }

    return (
        <div className={"flex-container"}>
            <div className={"qtr-fold mr-20"}><ClusterToolbar fetchPlot={fetchPlot}/></div>
            <div className={"qtr-fold-inv"}>
                {renderPlot()}
            </div>
        </div>
    )
}

export default ClusterTab;