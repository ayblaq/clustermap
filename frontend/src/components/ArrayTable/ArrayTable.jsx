import React from 'react';
import {Container, Table} from 'semantic-ui-react';
import './ArrayTable.scss'


const ArrayTable = (props) => {
    const {columns, rows} = props

    return (
        <Container>
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell/>
                        {columns.map((value, key) => {
                            return <Table.HeaderCell key={key}>{value}</Table.HeaderCell>
                        })}
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {Object.keys(rows).map((row, key) => {
                        return <Table.Row key={key}>
                            <Table.Cell>{ row }</Table.Cell>
                            {rows[row].map((value, index) => {
                                return <Table.Cell key={index}>{value}</Table.Cell>
                            })}
                        </Table.Row>
                    })}
                </Table.Body>
            </Table>
        </Container>
    )
}

export default ArrayTable;