import React, {useState} from 'react';
import {useSelector} from "react-redux";
import {JsonTable, PredictorToolbar} from "../index";
import {MessageBox, ProgressLoader} from "../../common/components";
import {fetchPrediction} from "../api/api";
import {arrayToObjects} from "../redux/actions";


const PredictorTab = () => {
    const [value, setValue] = useState({data: {}, loading: false, error: false})

    const {
        data: {title}
    } = useSelector(state => state.data)

    const columns = ['Origin', 'Destination', 'Modes']

    const predictFunc = (algo) => {
        setValue({...value, loading: true})
        if (title) {
            fetchPrediction(title, algo).then(res => {
                console.log(res.data)
                setValue({...value, loading: false, data: res.data})
                renderSection()
            }).catch(error => {
                console.log(error.error)
            })
        }
    }

    const renderSection = () => {
        if (!title) {
            return <MessageBox details={"Please Upload data"} type={"warning"}/>
        } else if (value.loading) {
            return <ProgressLoader description={"Making prediction"} size={"small"}/>
        } else if (Object.keys(value.data).length > 0) {
            let rows = arrayToObjects(columns, value.data)
            return <JsonTable rows={rows} columns={columns}></JsonTable>
        }
        return <div></div>
    }

    return (
        <div className={"flex-container"}>
            <div className={"qtr-fold mr-20"}><PredictorToolbar predictFunc={predictFunc}></PredictorToolbar></div>
            <div className={"qtr-fold-inv"}>
                {renderSection()}
            </div>
        </div>
    )
}

export default PredictorTab;