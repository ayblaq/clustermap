import React from 'react';
import {Container, Message, Icon, Header} from 'semantic-ui-react';
import {ArrayTable} from "../index";
import './StatTables.scss'


const StatTables = (prop) => {

    const {
        data, statistics
    } = prop

    const default_columns = Object.keys(data)

    const renderData = () => {
        return Object.keys(statistics).map((value, index) => {
            return <div key={index} className={'mt-10 mb-10'}><Header as='h5'>{value}</Header>
                {renderStatistics(statistics[value])}
            </div>
        })
    }

    const renderStatistics = (element) => {
        let columns = default_columns
        let stats = {...element}
        if (stats.headers) {
            columns = stats.headers
            delete stats.headers
        }

        return <ArrayTable rows={stats} columns={columns}></ArrayTable>
    }

    if (Object.keys(data).length <= 0) {
        return (
            <Message warning attached='bottom'>
                <Icon name='warning'/>
                No data loaded. Please upload your tranjectory dataset!
            </Message>
        )
    }
    return (
        <Container>{renderData()}</Container>
    )
}

export default StatTables;