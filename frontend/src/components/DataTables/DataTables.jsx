import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Container} from 'semantic-ui-react';
import {arrayToObjects, fetchAnalysisData} from "../redux/actions";
import {ProgressLoader} from "../../common/components";
import {JsonTable} from "../index";
import './DataTables.scss'


const DataTables = (props) => {

    const {options, main, id} = props

    const {
        analysis: {analysis, isFetching, error, msg}
    } = useSelector(state => state.analysis)

    const dispatch = useDispatch();

    useEffect(() => {
        if (Object.keys(analysis).length === 0) {
            dispatch(fetchAnalysisData(id))
        }
    }, [])

    const columns = ['Origin', 'Destination', ...Object.keys(options)]

    const renderTable = () => {
        let rows = arrayToObjects(columns, analysis)
        return <JsonTable rows={rows} columns={columns}></JsonTable>
    }

    return (
        <Container>
            {isFetching ? <ProgressLoader description={"fetching data"} size={"small"}/> : renderTable()}
        </Container>
    )
}

export default DataTables;