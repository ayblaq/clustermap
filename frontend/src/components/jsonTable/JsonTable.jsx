import React from 'react';
import {Container, Icon, Table} from 'semantic-ui-react';
import './JsonTable.scss'


const JsonTable = (props) => {
    const {columns, rows} = props

    return (
        <Container>
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        {columns.map((value, key) => {
                            return <Table.HeaderCell key={key}>{value}</Table.HeaderCell>
                        })}
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {rows.map((row, key) => {
                        return <Table.Row key={key}>
                                {columns.map((col, index) => {
                                    return <Table.Cell key={index}>{row[col]}</Table.Cell>
                                })}
                        </Table.Row>
                    })}
                </Table.Body>
            </Table>
        </Container>
    )
}

export default JsonTable;