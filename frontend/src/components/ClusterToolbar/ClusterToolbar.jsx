import React, {useState} from 'react';
import {Container, Header, Dropdown, Button, Icon, Radio} from 'semantic-ui-react';
import "./ClusterToolbar.scss"

const ClusterToolbar = (props) => {

    const {fetchPlot} = props
    const [option, setOption] = useState({active: 'kmeans'})

    const clusterAlgorithm = {
        'kmeans': {
            settings: [
                {
                    name: 'n_cluster',
                    min: 1,
                    max: 10,
                    title: 'Number of clusters',
                    key: 1
                }
            ],
            name: 'K-Means',
            status: true,
        },
        'dbscan': {
            settings: [
                {
                    name: 'epsilon',
                    min: 0.5,
                    max: 10,
                    title: 'Epsilon Value',
                    key: 2
                }
            ],
            name: 'DBSCAN',
            status: false
        },
        'pca': {
            settings: [
                {
                    name: 'n_cluster',
                    min: 1,
                    max: 10,
                    title: 'Number of clusters',
                    key: 3
                },
                {
                    name: 'n_component',
                    min: 2,
                    max: 2,
                    title: 'Number of components',
                    key: 4
                }
            ],
            name: 'PCA / K-Means',
            status: false
        }
    }

    const switchOption = (e, key) => {
        if (key !== option.active) {
            setOption({active: key})
        }
    }

    const switchSettings = (e, data, name) => {
        setOption({...option, [name]: data.value})
    }

    const renderSettings = () => {
        const current = clusterAlgorithm[option.active]

        return <div>
            {current.settings.map((value, key) => {
                return <div key={value.key} className={"mt-10 drop-box"}>
                    <div className={"header-title"}>{value.title}</div>
                    <Dropdown
                        placeholder='Select'
                        fluid
                        selection
                        options={Array(Math.ceil(value.max - value.min) + 1).fill().map((element, index) => {
                            return {key: index, value: index + value.min, text: index + value.min}
                        })}
                        onChange={(event, data) => switchSettings(event, data, value.name)}
                    />
                </div>
            })}
            <div className={"mt-10"}>
                <Button className={"btn-box"} onClick={event => fetchPlot(event, option)}>
                    Show Plot
                </Button>
            </div>
        </div>
    }

    return (
        <Container>
            <Header as='h4'>Cluster options</Header>
            {Object.keys(clusterAlgorithm).map((value, key) => {
                return <div key={key} className={"filter-box"}>
                    <Radio
                        label={clusterAlgorithm[value].name}
                        name='radioGroup'
                        value={value}
                        checked={option.active === value}
                        onChange={(event) => switchOption(event, value)}
                    />
                </div>
            })}
            {renderSettings()}
        </Container>
    )
}


export default ClusterToolbar;