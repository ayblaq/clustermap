import React, {useState} from 'react';
import {Container, Header, Dropdown, Checkbox} from 'semantic-ui-react';
import './PreprocessToolbar.scss'


const PreprocessToolbar = (props) => {
    const {fetchFunc} = props

    const distanceAlgorithm = [
        {
            key: 'gc',
            value: 'gc',
            text: 'Great Circle'
        },
        {
            key: 'hv',
            value: 'hv',
            text: 'Haversine'
        },
        {
            key: 'gd',
            value: 'gd',
            text: 'Geodesic'
        },
        {
            key: 'eu',
            value: 'eu',
            text: 'Euclidean'
        },
    ]

    const statOptions = [
        {
            label: "Show empty rows",
            status: true,
            key: "empty_row"
        },
        {
            label: "Show empty columns",
            status: true,
            key: "empty_col"
        },
    ]

    const filterOptions = [
        {
            label: "Filter empty rows",
            status: false,
            key: "filter_empty"
        },
        {
            label: "Apply Savitzky-Golay Smoothing",
            status: false,
            key: "filter_golay"
        },
    ]

    const computeOptions = [
        {
            label: "Distance",
            status: false,
            key: "Distance"
        },
        {
            label: "Velocity",
            status: false,
            key: "Velocity"
        },
        {
            label: "Bearing",
            status: false,
            key: "Bearing"
        },
        {
            label: "Acceleration",
            status: false,
            key: "Acceleration"
        },
        {
            label: "Jerk",
            status: false,
            key: "Jerk"
        },
        {
            label: "Crow Ratio",
            status: false,
            key: "Crowratio"
        },
        {
            label: "Crow Length",
            status: false,
            key: "crowlength"
        },
        {
            label: "Time Delta",
            status: false,
            key: "Time_delta"
        },
    ]

    return (
        <Container>
            <Header as='h4'>Pre-processing options</Header>
            <Header as='h5'>Statistics:</Header>
            {statOptions.map((value, key) => {
                return <div key={key} className={"filter-box"}>
                    <Checkbox label={`${value.label}`} defaultChecked={value.status ? true : false} disabled/>
                </div>
            })}
            <Header as='h5'>Filter Options:</Header>
            {filterOptions.map((value, key) => {
                return <div key={key} className={"filter-box"}>
                    <Checkbox label={`${value.label}`} defaultChecked={value.status ? true : false}/>
                </div>
            })}
            <Header as='h5'>Distance Algorithm:</Header>
            <div className={"drop-box"}>
                <Dropdown
                    fluid
                    selection
                    options={distanceAlgorithm}
                    defaultValue={distanceAlgorithm[0].value}
                />
            </div>
            <Header as='h5'>Show/Hide Options:</Header>
            {computeOptions.map((value, key) => {
                return <div key={key} className={"filter-box"}>
                    <Checkbox label={`${value.label}`} value={value.key} onClick={event => fetchFunc(event, value)}/>
                </div>
            })}
        </Container>
    )
}


export default PreprocessToolbar;