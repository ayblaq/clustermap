import {axiosClient} from '../../api';

export async function uploadFileApi(file) {
    let formData = new FormData();
    formData.append("file", file)
    return await axiosClient.post('/dataset', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    }).then((response) => {
        return response.data
    }).catch(error => {
        throw error;
    })
}

export async function fetchClusterPlotApi(title, data) {
    let formData = new FormData();
    Object.keys(data).map((value, key) => {
        formData.append([value], data[value])
    })
    return await axiosClient.post(`/cluster/${title}/`, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    }).then((response) => {
        return response.data;
    })
        .catch(error => {
            throw error
        })
}

export async function fetchPrediction(title, model) {
    return await axiosClient.get(`/predict/${title}/model/${model}`)
        .then((response) => {
            return response.data;
        })
        .catch(error => {
            throw error
        })
}

export async function fetchDefaultDataApi() {
    return await axiosClient.get(`/dataset`)
        .then((response) => {
            return response.data;
        })
        .catch(error => {
            throw error
        })
}

export async function fetchAnalysisApi(title) {
    return await axiosClient.get(`/dataset/${title}`)
        .then((response) => {
            return response.data;
        })
        .catch(error => {
            throw error
        })
}
