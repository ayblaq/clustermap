import React, {useRef} from 'react';
import {useDispatch} from "react-redux";
import {Container, Form, Button, Segment, Icon, Header, Radio} from 'semantic-ui-react';
import {uploadFile, uploadCancel} from "../redux/actions";
import {ProgressLoader} from "../../common/components";
import './UploaderTab.scss'

const UploaderTab = (props) => {
    const {data, display} = props
    const dispatch = useDispatch()

    const fileChange = (e) => {
        const filename = e.target.files[0].name;
        const file_size = e.target.files[0].size;
        const allowedExtensions = /(\.csv|\.xls|\.xlsv)$/i;
        let error = "";
        if (allowedExtensions.exec(filename)) {
            if (file_size > 2010000) {
                e.target.value = ""
                error = "File is greater than 2MB";
                dispatch(uploadCancel(error))
            } else {
                dispatch(uploadFile(e.target.files[0]))
            }
        } else {
            e.target.value = ""
            error = "File type is not allowed";
            dispatch(uploadCancel(error))
        }
    };

    const fileInputRef = useRef();
    if (!display) {
        return <div></div>
    }
    return (
        <Container>
            <Segment placeholder>
                <Header icon>
                    <Icon name='file outline'/>
                    <span className={"small-font"}> Only CSV allowed and maximum file size is 2MB </span>
                </Header>
                <Form>
                    <Form.Field>
                        <input
                            ref={fileInputRef}
                            type="file"
                            hidden
                            onChange={fileChange}
                        />
                        <Button
                            content="Choose File"
                            labelPosition="left"
                            icon="file"
                            onClick={() => fileInputRef.current.click()}
                        />
                    </Form.Field>
                    <div className={`text-center ${data.error ? 'error' : 'success'}`}> {data.msg} </div>
                </Form>
                <div>
                    {data.isUploading ? <ProgressLoader description={"uploading file"} size={"small"}/> : ''}
                </div>
            </Segment>
        </Container>
    )
}

export default UploaderTab;