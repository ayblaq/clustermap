import React, {useState} from 'react';
import './PredictorToolbar.scss'
import {Header, Radio} from "semantic-ui-react";

const PredictorToolbar = (props) => {

    const [option, setOption] = useState({algo: ''})

    const {predictFunc} = props;

    const dataOptions = [
        {
            key: 'knn',
            label: 'K-nearest neighbors'
        },
        {
            key: 'svm',
            label: 'Support Vector Machine'
        },
        {
            key: 'xgboost',
            label: 'XG Boosting'
        },
        {
            key: 'rforest',
            label: 'Random Forest'
        },
        {
            key: 'gboost',
            label: 'Gradient Boosting'
        }
    ]

    const switchOption = (e, key) => {
        setOption({algo: key})
        predictFunc(key)
    }

    return (
        <div>
            <div><Header as="h5">Choose Data Input Type</Header></div>
            <div className={"mt-10"}>
                {dataOptions.map((value, key) => {
                    return <div className={"mb-10"} key={key}>
                        <Radio
                            label={value.label}
                            name='radioGroup'
                            value={value.key}
                            checked={option.algo === value.key}
                            onChange={(event) => switchOption(event, value.key)}
                        />
                    </div>
                })}
            </div>
        </div>
    )
}

export default PredictorToolbar;