import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { dataReducer } from './DataReducer'
import { analysisReducer } from './AnalysisReducer'

const persistConfig = {
  key: 'data',
  storage: storage,
};

const dataReducers = (state = {}, action) => {
  return {
    data: dataReducer(state.data, action),
  }
};

const analysisReducers = (state = {}, action) => {
  return {
    analysis: analysisReducer(state.analysis, action)
  }
};

const reducer = persistReducer(persistConfig, analysisReducers);
const datareducer = persistReducer(persistConfig, dataReducers);

export { reducer as analysisReducers }
export { datareducer as dataReducers }
