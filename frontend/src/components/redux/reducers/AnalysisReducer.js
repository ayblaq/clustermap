import {
    FETCH_ANALYSIS_REQUEST,
    FETCH_ANALYSIS_SUCCESS,
    FETCH_ANALYSIS_CANCEL,
    RESET_ANALYSIS
} from '../actions/AnalysisAction'

export const analysisReducer = (state = {
    isFetching: true,
    analysis: {},
    msg: '',
    error: false
}, action) => {
    switch (action.type) {
        case FETCH_ANALYSIS_REQUEST:
            return {
                ...state,
                isFetching: true,
                error: false
            };
        case RESET_ANALYSIS:
            return {
                ...state,
                isFetching: false,
                error: false,
                analysis: {}
            };
        case FETCH_ANALYSIS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                analysis: {
                    ...action.payload.data,
                },
            };
        case FETCH_ANALYSIS_CANCEL:
            return {
                ...state,
                isFetching: false,
                error: true,
                msg: action.payload
            };
        default:
            return state
    }
}