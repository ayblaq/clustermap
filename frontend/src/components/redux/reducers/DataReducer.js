import {
    UPLOAD_REQUEST,
    UPLOAD_SUCCESS,
    UPLOAD_CANCEL
} from '../actions/DataAction'

export const dataReducer = (state = {
    isUploading: false,
    data: {},
    statistics: {},
    title: null,
    msg: '',
    error: false
}, action) => {
    switch (action.type) {
        case UPLOAD_REQUEST:
            return {
                ...state,
                isUploading: true,
                error: false
            };
        case UPLOAD_SUCCESS:
            return {
                ...state,
                isUploading: false,
                data: {
                    ...action.payload.data,
                },
                statistics: {
                    ...action.payload.statistics,
                },
                title: action.payload.title
            };
        case UPLOAD_CANCEL:
            return {
                ...state,
                isUploading: false,
                error: true,
                msg: action.payload
            };
        default:
            return state
    }
}