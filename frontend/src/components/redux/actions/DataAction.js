import {uploadFileApi, fetchDefaultDataApi} from "../../api/api";

export const UPLOAD_REQUEST = 'UPLOAD_REQUEST';
export const UPLOAD_SUCCESS = 'UPLOAD_SUCCESS';
export const UPLOAD_CANCEL = 'UPLOAD_CANCEL';

export const uploadRequest = () => ({
    type: UPLOAD_REQUEST,
    payload: {
        data: {}
    }
})

export const uploadSuccess = (data) => ({
    type: UPLOAD_SUCCESS,
    payload: data
})

export const uploadCancel = (error) => ({
    type: UPLOAD_CANCEL,
    payload: error
})

export const uploadFile = (file) => (dispatch) => {
    dispatch(uploadRequest())
    uploadFileApi(file).then(res => {
        dispatch(uploadSuccess(res))
    }).catch(error => {
        dispatch(uploadCancel(error.error))
    })
}

export const fetchDefault = () => (dispatch) => {
    dispatch(uploadRequest())
    fetchDefaultDataApi().then(res => {
        dispatch(uploadSuccess(res))
    }).catch(error => {
        dispatch(uploadCancel(error.error))
    })
}

export const arrayToObjects = (columns, data) => {
    let rows = []
    for (let i = 0; i < 10; i++) {
        let row = {}
        if (data[columns[0]][i]) {
            columns.map((value, key) => {
                row[value] = data[value][i]
            })
            rows.push(row)
        }
    }

    return rows;
}
