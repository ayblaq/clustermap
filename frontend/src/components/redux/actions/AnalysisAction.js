import {fetchAnalysisApi} from "../../api/api";

export const FETCH_ANALYSIS_REQUEST = 'FETCH_ANALYSIS_REQUEST';
export const FETCH_ANALYSIS_SUCCESS = 'FETCH_ANALYSIS_SUCCESS';
export const FETCH_ANALYSIS_CANCEL = 'FETCH_ANALYSIS_CANCEL';
export const RESET_ANALYSIS = 'RESET_ANALYSIS';

export const fetchRequest = () => ({
    type: FETCH_ANALYSIS_REQUEST,
    payload: {
        data: {}
    }
})

export const fetchSuccess = (data) => ({
    type: FETCH_ANALYSIS_SUCCESS,
    payload: data
})

export const fetchCancel = (error) => ({
    type: FETCH_ANALYSIS_CANCEL,
    payload: error
})

export const resetData = () => ({
    type: RESET_ANALYSIS
})


export const fetchAnalysisData = (title) => (dispatch) => {
    dispatch(fetchRequest())
    fetchAnalysisApi(title).then(res => {
        dispatch(fetchSuccess(res))
    }).catch(error => {
        dispatch(fetchCancel(error.error))
    })
}

