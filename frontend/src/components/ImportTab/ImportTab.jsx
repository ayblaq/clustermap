import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Container, Header, Radio} from 'semantic-ui-react';
import {JsonTable, UploaderTab} from "../index";
import {arrayToObjects, fetchDefault} from "../redux/actions";
import {ProgressLoader} from "../../common/components";
import './ImportTab.scss'

const ImportTab = () => {

    const {
        data
    } = useSelector(state => state.data)

    const [option, setOption] = useState({def: 'own'})

    const dispatch = useDispatch();

    const dataOptions = [
        {
            key: 'dummy',
            label: 'Load Dummy Data'
        },
        {
            key: 'own',
            label: 'Upload Data'
        },

    ]

    const switchOption = (e, key) => {
        setOption({def: key})
        if (key === "dummy") {
            dispatch(fetchDefault())
        }
    }

    const tableObject = () => {
        let columns = Object.keys(data.data)
        let rows = arrayToObjects(columns, data.data)
        const rowLength = data.data[columns[0]].length;

        return <div className={"mb-5"}><span
            className={"bold-font mb-5"}>Column annotations ({rowLength} rows, {columns.length} columns)</span>
            <JsonTable rows={rows} columns={columns}></JsonTable>
        </div>;
    }

    return (
        <Container>
            <div className={"flex-container"}>
                <div className={"qtr-fold"}>
                    <div><Header as="h5">Choose Data Input Type</Header></div>
                    <div className={"mt-10"}>
                        {dataOptions.map((value, key) => {
                            return <div className={"mb-10"} key={key}>
                                <Radio
                                    label={value.label}
                                    name='radioGroup'
                                    value={value.key}
                                    checked={option.def === value.key}
                                    onChange={(event) => switchOption(event, value.key)}
                                />
                            </div>
                        })}
                    </div>
                    <UploaderTab data={data} display={option.def === "own"}/>
                </div>
                <div className={"qtr-fold-inv ml-10"}>
                    {data.isUploading ? <ProgressLoader description={"fetching data"} size={"small"}/> : ''}
                    {Object.keys(data.data).length > 0 ? tableObject() : ''}
                </div>
            </div>
        </Container>
    )
}

export default ImportTab;