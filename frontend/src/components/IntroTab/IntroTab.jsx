import React from "react";
import {Container} from 'semantic-ui-react'
import './IntroTab.scss'
import imgModel from '../../images/model.png'

const IntroTab = () => {
    return (
        <Container>
            This web tool allows users to upload their trajectory dataset and easily create cluster plots.
            Data can be uploaded as a
            csv file using the Import tab.In addition, the tool maps transport mode to each tranjectory rows by using
            the internal transportation mode prediction tool. It tries to identify 4 main transporation modes: bike, vehicle, train, walk and bus.
            Data format is shown under "Help" tab.
            Several python packages are used internally, including pandas, numpy, scikit learn and gridSVG.
            It is developed in ITS Research Group and the source code is available in GitLab.
            Please submit bug reports and feature requests to GitHub issues page or send to adewale [at] ut.ee.
        </Container>
    )
}

export default IntroTab;