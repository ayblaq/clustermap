import React, {useState} from 'react';
import {useSelector} from "react-redux";
import {Message, Icon} from 'semantic-ui-react';
import {StatTables, PreprocessToolbar, DataTables} from "../index";
import './PreprocessTab.scss'


const PreprocessTab = () => {
    const {
        data: {data, statistics, title}
    } = useSelector(state => state.data)

    const [options, setOptions] = useState({})

    const renderTables = () => {
        if (Object.keys(data).length <= 0) {
            return <Message warning attached='bottom'>
                <Icon name='warning'/>
                No data loaded. Please upload your trajectory dataset!
            </Message>
        } else {
            return <div><StatTables data={data} statistics={statistics}/><DataTables options={options} main={data} id={title}/>
            </div>
        }
    }

    const getData = (event, value) => {
        if (options[value.key]) {
            delete options[value.key]
        } else {
            options[value.key] = true
        }

        setOptions({...options})
    }

    return (
        <div className={"flex-container"}>
            <div className={"qtr-fold mr-20"}><PreprocessToolbar fetchFunc={getData}/></div>
            <div className={"qtr-fold-inv"}>
                {renderTables()}
            </div>
        </div>
    )
}

export default PreprocessTab;