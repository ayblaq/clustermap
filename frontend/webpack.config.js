module.exports = {
    externals: {
        application: "application"
    },
    watch: true,
    watchOptions: {
        ignored: /node_modules/
    },
    entry: '../src/index.js',
    node: {fs: 'empty', net: 'empty', tls: 'empty', child_process: 'empty', __filename: true, __dirname: true},
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.jsx?$/,
                loader: "babel-loader",
                exclude: [/node_modules/],
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.s[a|c]ss$/,
                use: [
                    "style-loader", "css-loader", "sass-loader"
                ]
            },
            {
                test: /\.(jpg|png|gif|eot|ttf|svg)$/,
                loader: 'url-loader?limit=100000'
            },
            {
                test: /\.(eot|ttf|svg)$/,
                use: {
                    loader: 'url-loader',
                    query: {
                        limit: 30000,
                        name: '[name].[ext]',
                    },
                },
            },
            {
                test: /\.(woff|woff2)$/,
                use: 'url-loader?limit=100000&mimetype=application/font-woff',
            },
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: ['node_modules/'],
        descriptionFiles: ['package.json']
    },
    output: {
        publicPath: "http://127.0.0.1:8000/", // Development Server
        // publicPath: "http://example.com/", // Production Server
    }
};
