import numpy as np
import itertools as iterr
from dateutil.parser import parse
import time


def remove_null(df):
    df = df.dropna()
    df = df.dropna(axis=1)
    return df


def is_time(value):
    try:
        if time.strptime(value, '%H:%M:%S') or time.strptime(value, '%H:%M'):
            return True
    except ValueError:
        return False


def is_date_time(value):
    try:
        parse(value, fuzzy=True)
        return True

    except ValueError:
        return False


def is_date(value):
    try:
        if time.strptime(value, "%Y-%m-%d") or time.strptime(value, "%Y/%m/%d")\
                or time.strptime(value, "%Y %m %d"):
            return True
    except ValueError:
        return False


def get_date_col(df):
    new_df = df.select_dtypes(include=['object', 'datetime64'])
    cols = new_df.columns
    date_col = {}
    for col in cols:
        if df[col].dtype == np.object:
            if is_time(df[col].head(1).values[0]):
                date_col['time'] = col
            elif is_date(df[col].head(1).values[0]):
                date_col['date'] = col
            elif is_date_time(df[col].head(1).values[0]):
                date_col['datetime'] = col

    return date_col


def check_column_type(df):
    cols = df.columns
    result = {'error':False, 'message': ''}
    for col in cols:
        if df[col].dtype != np.float or df[col].dtype != np.int64 \
                or df[col].dtype != np.datetime64:
            result = {'error': True, 'message': 'Invalid rows found'}

    new_df = df.select_dtypes(include=['int64', 'float64'])
    cols = new_df.columns
    if len(cols) <= 1:
        return {'error': True, 'message': 'No Latitude and Longitude Column'}

    return result


def validate_coordinate(lat, lon):
    if lat >= -90 and lat <= 90 and lon >= -180 and lon <= 180:
        return True
    else:
        return False


def get_lat_lon_col(df):
    new_df = df.select_dtypes(include=['int64', 'float64'])
    cols = new_df.columns
    lat_lon = []
    for pair in iterr.permutations(cols, 2):
        pair = list(pair)
        valid_pair = validate_coordinate(df[pair[0]].head(1).values[0], df[pair[1]].head(1).values[0])
        if valid_pair:
            lat_lon = list(pair)
            break
    return lat_lon


def validate_file(request):
    csv_file = request.FILES['file']
    if not csv_file.name.endswith('.csv'):
        return False
    return True
