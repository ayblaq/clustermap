from data_analyse.data_cluster.clusters import kmeans_cluster
from data_analyse.data_cluster.clusters import dbscan_cluster
from data_analyse.data_cluster.clusters import pca_cluster


def cluster(cluster_name, request, dfObj):
    data = dfObj[['lat', 'long']].values
    dfObj['Longitude'] = dfObj['long']
    dfObj['Latitude'] = dfObj['lat']
    if cluster_name.lower() == "kmeans":
        image = kmeans_cluster.cluster(data, dfObj, request.get("n_cluster"))
    elif cluster_name.lower() == "pca":
        image = pca_cluster.cluster(data, request.get("n_cluster"))
    else:
        image = dbscan_cluster.cluster(dfObj, request.get("epsilon"))

    return image