from kneed import KneeLocator
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn import metrics
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from data_analyse.data_cluster.clusters.kmeans_cluster import get_cluster_number
from data_plot.data_plot import plot_centroid_circles


def cluster(data, n_clusters=None):
    pca = PCA(n_components=2)
    reduced_train_data = pca.fit_transform(data)
    if n_clusters is None:
        n_clusters = get_cluster_number(reduced_train_data)
    n_clusters = int(n_clusters)
    kmean = KMeans(n_clusters=n_clusters).fit(reduced_train_data)
    centroids = kmean.cluster_centers_
    new_df = pd.DataFrame({'Latitude': reduced_train_data[:, 0], 'Longitude': reduced_train_data[:, 1]})

    return plot_centroid_circles(new_df, centroids, kmean.labels_, "PCA / K-Means Cluster", ['PCA X TRANSFORM', 'PCA Y TRANSFORM'])


