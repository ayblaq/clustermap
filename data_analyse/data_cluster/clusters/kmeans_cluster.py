from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from data_plot.data_plot import plot_centroid_circles

from kneed import KneeLocator

plt.style.use('ggplot')
Ks = range(1, 10)


def cluster(data, df, n_clusters=None):
    if n_clusters is None:
        n_clusters = get_cluster_number(data)
    n_clusters = int(n_clusters)
    kmean = KMeans(n_clusters=n_clusters).fit(data)
    centroids = kmean.cluster_centers_

    return plot_centroid_circles(df, centroids, kmean.labels_, "K-Means Cluster")


def get_cluster_number(data):
    y = []
    for ki in Ks:
        kmeanModel = KMeans(n_clusters=ki)
        kmeanModel.fit(data)
        y.append(kmeanModel.inertia_)
    kn = KneeLocator(Ks, y, curve='convex', direction='decreasing')

    return kn.elbow