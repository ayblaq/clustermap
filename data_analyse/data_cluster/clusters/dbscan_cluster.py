import numpy as np
import pandas as pd
from sklearn.cluster import DBSCAN
from data_plot.data_plot import plot_centroid_circles


def cluster(df, epsilon = None):
    # convert epsilon to radians
    if epsilon is None:
        epsilon = 0.5
    epsilon = float(epsilon) / 6271
    data = df[['Latitude', 'Longitude']].values
    dbscan = (DBSCAN(eps=epsilon, min_samples=5, metric='euclidean').fit(np.radians(data)))
    labels = dbscan.labels_
    num_of_clusters =  len(set(labels))
    dbscan_series = pd.Series([data[labels == n, :] for n in range(num_of_clusters)])
    centroids = dbscan_series.map(calc_centroid)

    return plot_centroid_circles(df, centroids, labels, "DBSCAN Cluster")


def calc_centroid(data):
    centroid = np.asarray(data).mean(axis=0)
    return centroid
