import pandas as pd
import numpy as np
from data_analyse.data_filter import data_filter
import vincenty
from numpy import arctan2, random, sin, cos, degrees
import os


def recreate_df_cols(title, df):
    filename = os.getcwd() + '/dataset/formatted/{title}.csv'.format(title=title)
    if os.path.isfile(filename):
        return pd.read_csv(filename)
    new_df = pd.DataFrame()
    lat_long = data_filter.get_lat_lon_col(df)
    date_col = data_filter.get_date_col(df)

    if len(lat_long) > 1:
        new_df['Latitude'] = df[lat_long[0]]
        new_df['Longitude'] = df[lat_long[1]]

    if date_col['time'] and date_col['date']:
        new_df['DateTime'] \
            = df[[date_col['date'], date_col['time']]].agg(' '.join, axis=1)

    elif date_col['datetime']:
        new_df['DateTime'] = df[date_col['datetime']]

    if 'DateTime' in new_df:
        new_df['DateTime'] = new_df['DateTime'].apply(pd.to_datetime)

    new_df = create_data_step(new_df)
    new_df = add_bearing(new_df)
    new_df = add_distance_col(new_df)
    new_df = add_velocity_col(new_df)
    new_df = add_acceleration_col(new_df)
    new_df = add_pair_wise(new_df)
    new_df = new_df.dropna()

    aggregate_attr(title, new_df)
    return new_df


def aggregate_attr(title, data):
    if 'Velocity' in data and 'Acceleration' in data:
        data.loc[:, 'Vel_avg'] = round(np.nanmean(data['Velocity'].values), 2)
        data.loc[:, 'Vel_med'] = round(np.nanmedian(data['Velocity'].values), 2)
        data.loc[:, 'Vel_max'] = round(np.nanmax(data['Velocity'].values), 2)
        data.loc[:, 'Vel_std'] = round(np.nanstd(data['Velocity'].values), 2)

        data.loc[:, 'Accl_avg'] = round(np.nanmean(data['Acceleration'].values), 2)
        data.loc[:, 'Accl_med'] = round(np.nanmedian(data['Acceleration'].values), 2)
        data.loc[:, 'Accl_max'] = round(np.nanmax(data['Acceleration'].values), 2)
        data.loc[:, 'Accl_std'] = round(np.nanstd(data['Acceleration'].values), 2)

        data.loc[:, 'Jerk_avg'] = round(np.nanmean(data['Jerk'].values), 2)
        data.loc[:, 'Jerk_med'] = round(np.nanmedian(data['Jerk'].values), 2)
        data.loc[:, 'Jerk_max'] = round(np.nanmax(data['Jerk'].values), 2)
        data.loc[:, 'Jerk_std'] = round(np.nanstd(data['Jerk'].values), 2)

        data.loc[:, 'Velocity_change_avg'] = round(np.nanmean(data['Velocity_change'].values), 2)
        data.loc[:, 'Velocity_change_med'] = round(np.nanmedian(data['Velocity_change'].values), 2)
        data.loc[:, 'Velocity_change_max'] = round(np.nanmax(data['Velocity_change'].values), 2)
        data.loc[:, 'Velocity_change_std'] = round(np.nanstd(data['Velocity_change'].values), 2)

        data.to_csv(os.getcwd() + "/dataset/formatted/{file}.csv".format(file=title), index=False)


def add_pair_wise(df):
    df['Origin'] = df.apply(lambda x: "({}, {})".format(x.Latitude, x.Longitude), axis=1)
    df['Destination'] = df.apply(lambda x: "({}, {})".format(x.Latitude_next, x.Longitude_next), axis=1)
    df = df.drop(['Latitude_next', 'Longitude_next'], axis=1)

    return df


def add_bearing(df):
    if 'Latitude' in df and 'Longitude' in df:
        df['Bearing'] = df.apply(lambda x: compute_bearing(x.Latitude, x.Longitude, x.Latitude_next, x.Longitude_next),
                                 axis=1)

    return df


def add_distance_col(df):
    if 'Latitude' in df and 'Longitude' in df:
        df['Distance'] = df.apply(lambda x: compute_distance(x.Latitude, x.Longitude,
                                                             x.Latitude_next, x.Longitude_next), axis=1)

        df['crowlength'] = df.apply(lambda x: compute_crowlength(x.Latitude, x.Longitude,
                                                                 x.Latitude_next, x.Longitude_next), axis=1)

        df['Crowratio'] = df.apply(lambda x: compute_crowratio(x.Distance, x.crowlength), axis=1)

    return df


def compute_crowratio(distance, crowlength):
    return round((distance / crowlength), 2)


def compute_crowlength(lat, long, next_lat, next_long):
    crow_length = ((long - next_long) ** 2) + ((lat + next_lat) ** 2)
    return round(crow_length, 2)


def compute_bearing(lat, long, next_lat, next_long):
    X = cos(next_lat) * sin(next_long - long)
    Y = cos(lat) * sin(next_lat) - sin(lat) * cos(next_lat) * cos(next_long - long)

    return round(arctan2(X, Y), 2)


def add_velocity_col(df):
    if 'Distance' in df and 'DateTime' in df:
        df['Time_delta'] = df.apply(lambda x: (x.DateTime_next - x.DateTime).total_seconds(), axis=1)
        df['Velocity'] = df.apply(lambda x: compute_velocity(x.Distance, x.Time_delta), axis=1)
        df['Velocity_next'] = df['Velocity'].shift(-1)

        df['Velocity_change'] = df.apply(lambda x: velocity_change_rate(x.Velocity, x.Velocity_next, x.Distance),
                                         axis=1)

    return df


def velocity_change_rate(velocity, velocity_next, distance):
    try:
        velocity_change = ((velocity_next - velocity) / velocity) / distance
        return round(velocity_change, 2)
    except BaseException:
        return np.nan


def add_acceleration_col(df):
    if 'Velocity' in df and 'Time_delta' in df:
        df['Acceleration'] = df.apply(lambda x: compute_acceleration(x.Velocity, x.Velocity_next, x.Time_delta), axis=1)
        df['Acceleration_next'] = df['acceleration'].shift(-1)

        df['Jerk'] = df.apply(lambda x: compute_jerk_rate(x.Acceleration, x.Acceleration_next, x.Time_delta), axis=1)

        df = df.drop(['Velocity_next', 'DateTime_next', 'Acceleration_next'], axis=1)

    return df


def compute_jerk_rate(acceleration, acceleration_next, timedelta):
    try:
        return round((acceleration_next - acceleration) / timedelta, 2)
    except BaseException:
        return np.nan


def compute_acceleration(velocity, next_velocity, timedelta):
    if timedelta == 0:
        return np.nan

    return round((next_velocity - velocity) / timedelta, 2)


def compute_distance(lat, long, next_lat, next_long):
    if lat == next_lat and long == next_long:
        return 0
    if False in np.isfinite([long, next_long, lat, next_lat]):
        return np.nan
    if lat < -90 or lat > 90 or next_lat < -90 or next_lat > 90:
        return np.nan
    if long < -180 or long > 180 or next_long < -180 or next_long > 180:
        return np.nan
    distance = vincenty.vincenty((lat, long), (next_lat, next_long), miles=True)

    return round(distance * 1609, 2)


def compute_velocity(distance, timedelta):
    try:
        return round((distance / timedelta), 2)
    except BaseException:
        return np.nan


def create_data_step(df, step=1):
    if 'Longitude' in df:
        df['Longitude_next'] = df['Longitude'].shift(-step)
    if 'Latitude' in df:
        df['Latitude_next'] = df['Latitude'].shift(-step)
    if 'DateTime' in df:
        df['DateTime_next'] = df['DateTime'].shift(-step)

    return df
