import pandas as pd
from data_analyse.data_filter import data_filter

def null_stats(df):
    data = df.isna().sum()
    data_percent = 100 * data / len(df)
    null_data = pd.concat([data, data_percent], axis=1)
    null_data = null_data.rename(
        columns={0: 'NaN Values', 1: '% of Total Values'})
    return null_data


def data_matrix_stats(df):
    new_df = data_filter.remove_null(df)
    headers = ['Before Processing', 'After Removing Columns and Rows with NA']
    return {'Rows': [len(df), len(new_df)], 'Columns': [len(df.columns), len(new_df.columns)], 'headers': headers}


def describe_stats(df):
    df.dropna(inplace=True)
    percentile = [.20, .40, .60, .80]
    include = ['datetime', 'float', 'int']
    return df.describe(percentiles=percentile, include=include)